import couchdb
import json
from settings import Settings


from log import LocalLogger

FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
LOG = LocalLogger('couch')

class CouchManager(object):

    def __init__(self, database):
        self.user = Settings['couchdb_user']
        self.pw = Settings['couchdb_password']
        self.serverString = "http://%s:%s@localhost:5984"
        LOG.info("connecting to DB: %s" % (self.serverString))
        self.server = couchdb.Server(self.serverString % (self.user, self.pw))
        self.db = self.server[database]

    def getFormID(self, doc):
        try:
            _id = doc.get("id")
            return _id
        except Exception ,e:
            LOG.error(e)
            return None

    def getCaseID(self, doc):
        try:
            _id = doc.get('case_id')
            return _id
        except Exception ,e:
            LOG.error(e)
            return None

    def getCaseType(self, doc):
        try:
            props = doc.get('properties')
            return props.get('case_type')
        except Exception ,e:
            LOG.error(e)
            print json.dumps(doc, indent=4)
            return None

    def getUpdated(self, doc):
        try:
            mod = doc.get("date_modified")
            return mod
        except Exception ,e:
            LOG.error("Couldn't get date for doc : %s" % e)
            return None

    def setCaseID(self, doc):
        doc["_id"] = self.getCaseID(doc)
        return doc

    def setFormID(self,doc):
        doc["_id"] = self.getFormID(doc)
        return doc

    def replicate(self, source, target, options):

        sourceStr = self.serverString % (self.user, self.pw)+"/"+source
        targetStr = self.serverString % (self.user, self.pw)+"/"+target
        self.server.replicate(sourceStr, targetStr, **options)

    def addDoc(self, _id, doc, update=False):
        try:
            if _id in self.db:
                LOG.debug("doc with _id %s already exists" % (_id))
                if not update:
                    return False
                else:
                    try:
                        old_doc = self.db[_id]
                        old_mod = self.getUpdated(old_doc)
                        if old_mod == self.getUpdated(doc):
                            LOG.info("Case %s is current in db with update %s. Ignoring." % (_id, old_mod))
                            return False
                        _rev = old_doc.get("_rev")
                        doc["_rev"] = _rev
                        LOG.debug("updating... from _rev %s" % (_rev))
                        LOG.debug("Stale Doc\n%s" % (json.dumps(old_doc)))
                        LOG.debug("New Doc\n%s" % (json.dumps(doc)))
                    except Exception, e2:
                        LOG.error("Couldn't update doc %s: %s" % (_id, e2))
                        return False
            #save doc
            LOG.debug("Saving doc with id: %s" % _id)
            self.db[_id] = doc
            return True
        except Exception, e:
            LOG.debug("problem adding doc with _id %s: %s" % (_id, e))
            return False

    def addOrUpdateDoc(self, _id, doc):
        self.addDoc(_id, doc, update=True)

    def addForm(self, doc):
        doc['doc_type'] = "form"
        _id = self.getFormID(doc)
        if _id:
            return self.addDoc(_id, self.setFormID(doc))
        return False

    def addCase(self, doc):
        doc['doc_type'] = "case"
        _id = self.getCaseID(doc)
        case_type = self.getCaseType(doc)
        if not case_type: LOG.error("Couldn't get case_type from case: %s" % (json.dumps(doc, indent=4),))
        doc['case_type'] = case_type
        if _id:
            return self.addOrUpdateDoc(_id, self.setCaseID(doc))
        else:
            LOG.error("Case had no CaseID")
            raise ValueError("No CaseID!")
        return False



