import json

from couch import CouchManager
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.security import unauthenticated_userid, effective_principals
from pyramid.view import view_config
from log import LocalLogger

FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
LOG = LocalLogger('server')

CASES = CouchManager("case")
FORMS = CouchManager("form")

@view_config(
    route_name='home',
    renderer='json'
    )
def my_view(request):
    if not "system.Authenticated" in effective_principals(request):
        return {"status": "failed"}
    return {'status': 'ok'}

#FORM
@view_config(
    route_name='form',
    renderer='json'
    )
def form_view(request):
    if not "system.Authenticated" in effective_principals(request):
        return HTTPUnauthorized()
    try:
        jsonContent = request.json_body
        LOG.debug("New Form")
        #LOG.debug(json.dumps(jsonContent, indent=4))
        ok = FORMS.addForm(jsonContent)
        if ok:
            LOG.info("Added New Form")
        else:
            LOG.info("Couldn't add form!")
    except ValueError ,ve:
        LOG.error(ve)
    except Exception,e:
        LOG.error("%s |->\n%s" % (e,json.dumps(jsonContent, indent=2),))
    return {'result': 'ok'}

#CASE
@view_config(
    route_name='case',
    renderer='json'
    )
def case_view(request):
    if not "system.Authenticated" in effective_principals(request):
        return HTTPUnauthorized()
    try:
        jsonContent = request.json_body
        LOG.debug("New Case")
        #LOG.debug(json.dumps(jsonContent, indent=4))
        ok = CASES.addCase(jsonContent)
        if ok:
            LOG.info("Added New Case")
        else:
            LOG.info("Couldn't add case")
    except Exception,e:
        LOG.error("%s |->\n%s" % (e,json.dumps(jsonContent, indent=2),))
    return {'result': 'ok'}
