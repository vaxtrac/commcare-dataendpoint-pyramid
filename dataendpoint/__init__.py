from pyramid.config import Configurator
from pyramid.session import UnencryptedCookieSessionFactoryConfig
from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy

from log import LocalLogger
from settings import init_settings, Settings

LOG = LocalLogger('root-logger')

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    LOG.info("Starting Endpoint Server")
    init_settings(settings)
    from auth import credentialsCheck
    authn_policy = BasicAuthAuthenticationPolicy(credentialsCheck)
    authz_policy = ACLAuthorizationPolicy()
    default_session_factory = UnencryptedCookieSessionFactoryConfig(settings['cookie_secret'])
    config = Configurator(
        settings=settings,
        authentication_policy=authn_policy,
        )
    config.set_session_factory(default_session_factory)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('form', '/form')
    config.add_route('case', '/case')
    config.scan()
    return config.make_wsgi_app()
