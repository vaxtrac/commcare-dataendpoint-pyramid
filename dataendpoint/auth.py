from log import LocalLogger
from settings import Settings

LOG = LocalLogger('auth')

USERS = [Settings['site_username']]
PWS = {"commcare":Settings['site_password'],}

def credentialsCheck(userid, pw , request):
    #LOG.debug("%s, %s, %s" % (userid, pw, request,))
    ok = ( PWS.get(userid) == pw)
    if not ok:
        return None
    else:
        return ["authorized_users"]
