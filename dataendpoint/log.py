import logging
import os

APPNAME = "commcare-endpoint"
try:
    orig_user = os.environ['USER']
except Exception,e:
    orig_user = None
XTRAS = {'clientip': "127.0.0.1" }

class LocalLogger(object):

    names = []
    logcount = 0
    unifiedLog = None

    def __init__(self, name):
        LocalLogger.logcount = LocalLogger.logcount + 1
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        if LocalLogger.logcount <=1:
            LocalLogger.names = [name]
            logger = logging.getLogger(APPNAME)
            logger.setLevel(logging.DEBUG)


            fh = logging.FileHandler('unifiedlog_%s.txt' % APPNAME)
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(formatter)
            logger.addHandler(fh)
            LocalLogger.unifiedLog = logger
        else:
            LocalLogger.names.append(name)
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

        fh = logging.FileHandler('log_%s.txt' % name)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        self.name = name
        self.log = logger

    def getName(self):
        mL = max([len(n) for n in LocalLogger.names])
        return "%s%s" % (" "*(mL-len(self.name)),self.name)

    def debug(self, msg):
        self.log.debug(msg)
        LocalLogger.unifiedLog.debug("%s | %s" % (self.getName(), msg))

    def error(self, msg):
        self.log.error(msg)
        LocalLogger.unifiedLog.error("%s | %s" % (self.getName(), msg))

    def info(self, msg):
        self.log.info(msg)
        LocalLogger.unifiedLog.info("%s | %s" % (self.getName(), msg))

    def warn(self, msg):
        self.log.warning(msg)
        LocalLogger.unifiedLog.warning("%s | %s" % (self.getName(), msg))

